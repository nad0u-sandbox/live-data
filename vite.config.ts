import { fileURLToPath, URL } from "node:url";
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { VitePWA } from "vite-plugin-pwa";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      registerType: "autoUpdate",
      // strategies: 'injectManifest',
      // srcDir: "public",
      // filename: "service-worker.js",
      injectRegister: "auto",
      manifest: {
        name: "Live data",
        short_name: "Live data",
        description: "Live data",
        theme_color: "#2c3e50",
        background_color: "#2c3e50",
        display: "standalone",
        icons: [
          {
            "src": "./icon.png",
            "sizes": "512x512",
            "type": "image/png",
            "purpose": "any maskable"
          },
        ]
      },
      // injectManifest: {
      //   swSrc: 'public/service-worker.js',
      //   swDest: 'dist/sw.js'
      // },
      workbox: {
        importScripts: ["service-worker.js"],
        cleanupOutdatedCaches: true,
        globPatterns: ["**/*.{js,css,html,ico,png,svg,json,vue,txt,woff2}"]
      }
      // workbox: {
      //   cleanupOutdatedCaches: true,
      //   globPatterns: ["**/*.{js,css,html,ico,png,svg,json,vue,txt,woff2}"],
        
      // }
    })
  ],
  base: "/live-data/",
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
});


